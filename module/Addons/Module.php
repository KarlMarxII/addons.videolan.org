<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Addons;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Addons\Model\AddonsTable;
use Addons\Model\AddonsFileTable;
use Addons\Model\Addon;
use Addons\Model\AddonFile;

class Module implements ServiceProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
     
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Addons\Model\AddonsTable' => function($sm)
                {
                    $tableGateway = $sm->get('AddonsTableGateway');
                    $table = new AddonsTable($tableGateway);
                    return $table;
                },
                'Addons\Model\AddonsFileTable' => function($sm)
                {
                    $tableGateway = $sm->get('AddonsFilesTableGateway');
                    $table = new AddonsFileTable($tableGateway);
                    return $table;
                },
                'AddonsTableGateway' => function ($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Addon());
                    return new TableGateway('entries', $dbAdapter, null, $resultSetPrototype);
                },
                'AddonsFilesTableGateway' => function ($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AddonFile());
                    return new TableGateway('files', $dbAdapter, null, $resultSetPrototype);
                },
                /* https://github.com/ZF-Commons/ZfcUser/issues/110 */
                'zfcuser_user_mapper' => function ($sm) {
                    $options = $sm->get('zfcuser_module_options');
                    $mapper = new \ZfcUser\Mapper\User();
                    $mapper->setDbAdapter($sm->get('zfcuser_zend_db_adapter'));
                    $entityClass = $options->getUserEntityClass();
                    $mapper->setEntityPrototype(new $entityClass);
                    $mapper->setHydrator(new Mapper\UserHydrator());
                    return $mapper;
                },
            ),
        );
    }

}
