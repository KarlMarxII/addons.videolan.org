<?php
namespace Addons\Filter;

use Zend\Filter\AbstractFilter;
use \Imagick;

class PNGResizer extends AbstractFilter
{
    protected $width = 48;
    protected $height = 48;

    public function __construct( $options = null )
    {
        $this->setOptions($options);
    }
    
    public function setWidth( $value )
    {
        $this->width = $value;
    }

    public function setHeight( $value )
    {
        $this->height = $value;
    }

    public function filter($data)
    {
        $im = new Imagick();
        $im->readimageblob( $data );
        if ( !$im->valid() )
            return null;
        $im->thumbnailImage( $this->width, $this->height, true );
        $im->setCompressionQuality(60);
        $im->stripImage();
        $data = $im->getimageblob();
        return $data;
    }    
}