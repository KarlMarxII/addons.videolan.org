<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Addons\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Addons\Form\UploadAddonForm;
use Addons\Form\EditAddonForm;
use Addons\Form\EditAddonFilesForm;

class EditionController extends AbstractActionController
{
    protected $addonsTable;
    protected $addonsFileTable;
    protected $navigation;

    public function updateNavLabels( $currentname )
    {
        foreach( array('edit', 'editdesc', 'editfiles') as $id )
        {
            $page = $this->navigation->findBy('id', $id);
            if ( $page )
            {
                $page->setLabel( str_replace( '%1', $currentname, $page->getLabel() ) );
            }
        }
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->navigation = $this->getServiceLocator()->get('navigation');

        if ( $this->params('uuid') )
        {

            $page = $this->navigation->findBy('id', 'UserAddonsRoot');

            $editpage = new \Zend\Navigation\Page\Mvc(
                    array(
                        'id' => 'edit',
                        'label' => '"%1"',
                        'route' => 'addondetails/edit',
                        'params' => array('uuid' => $this->params('uuid') )
                    )
            );
            $editpage->setRouter( $e->getRouter() );
            $editpage->setRouteMatch( $e->getRouteMatch() );

            $descpage = new \Zend\Navigation\Page\Mvc(
                    array(
                        'id' => 'editdesc',
                        'label' => 'Description',
                        'route' => 'addondetails/editdesc',
                        'params' => array('uuid' => $this->params('uuid') )
                    )
            );
            $descpage->setRouter( $e->getRouter() );
            $descpage->setRouteMatch( $e->getRouteMatch() );

            $filespage = new \Zend\Navigation\Page\Mvc(
                array(
                'id' => 'editfiles',
                'label' => 'Files',
                'route' => 'addondetails/editfiles',
                'params' => array('uuid' => $this->params('uuid'))
            ));
            $filespage->setRouter( $e->getRouter() );
            $filespage->setRouteMatch( $e->getRouteMatch() );

            $editpage->addPage( $filespage );
            $editpage->addPage( $descpage );
            $page->addPage( $editpage );
        }

        if (!$this->zfcUserAuthentication()->hasIdentity())
        {
            $options = array( 'query' => array('redirect' => urlencode($e->getRequest()->getUriString())) );
            return $this->redirect()->toRoute("zfcuser/login", array(), $options);
        }

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        if ( !$this->addonsTable )
        {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }

        $addons = $this->addonsTable->getAddonsByUser(
                $this->zfcUserAuthentication()->getIdentity()->getId() );

        return array('addons' => $addons);
    }

    public function listAction()
    {
        if ( !$this->addonsTable )
        {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }

        $addons = $this->addonsTable->getAddonsByUser(
                $this->zfcUserAuthentication()->getIdentity()->getId() );
        return array('addons' => $addons);
    }

    protected function checkOwner($uuid, $addon)
    {
        if ( !$addon )
        {
            if ( !$this->addonsTable )
            {
                $sm = $this->getServiceLocator();
                $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
            }

            $addon = $this->addonsTable->getAddon($uuid);
        }

        if ( ! $this->isAllowed( $addon, 'modify' ) )
            throw new \Exception("Trying to edit unowned addon");
    }

    public function newAction()
    {
        if ( ! $this->isAllowed( 'addon', 'create' ) )
        {
            $options = array( 'query' => array('redirect' => urlencode($this->getRequest()->getUriString())) );
            return $this->redirect()->toRoute("zfcuser/login", array(), $options);
        }

        if ( !$this->addonsTable )
        {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }
        $addon = new \Addons\Model\Addon();

        $form = new EditAddonForm();
        $form->get('submit')->setLabel("Create new addon");
        $request = $this->getRequest();

        $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();

        if ( $request->isPost() )
        {
            $postdata = array_merge_recursive( $request->getPost()->toArray(),
                                               $request->getFiles()->toArray() );
            $form->setData($postdata);

            if ( $form->isValid() )
            {
                /* merge filtered */
                $hydrator->hydrate( $form->getInputFilter()->getValues(), $addon );

                if ( ($thumbnailfile = $form->getInputFilter()->getValue('thumbnail'))
                      && $thumbnailfile['tmp_name'] )
                {
                    $thumbnail = file_get_contents( $thumbnailfile['tmp_name'] );
                    $resizer = new \Addons\Filter\PNGResizer( array( 'width' => 256, 'height' => 256 ));
                    $addon->imagedata = $resizer->filter($thumbnail);
                }

                $addon->ref_userid = $this->zfcUserAuthentication()->getIdentity()->getId();
                $this->addonsTable->saveAddon( $addon );
                $uuid = $this->addonsTable->getLastInsertValue();
                return $this->redirect()->toRoute("addondetails/editdesc", array('uuid'=>$uuid));
            }
        }

        $viewModel = new \Zend\View\Model\ViewModel(
                array( 'addon' => $addon, 'form' => $form ) );
        $viewModel->setTemplate('addons/edition/editdesc.phtml');
        return $viewModel;
    }

    public function editAction()
    {
        if ( !$this->addonsTable || !$this->addonsFileTable )
        {
            $sm = $this->getServiceLocator();
            if ( !$this->addonsTable )
                $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
            if ( !$this->addonsFileTable )
                $this->addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
        }

        $addon = $this->addonsTable->getAddon( $this->params('uuid') );

        $this->checkOwner($this->params('uuid'), $addon);

        $this->updateNavLabels( $addon->name );

        $addonfiles = $this->addonsFileTable->fetch( $addon->uuid );

        return array('addon' => $addon, 'addonfiles' => $addonfiles );
    }

    public function editdescAction()
    {
        if ( !$this->addonsTable )
        {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }

        $addon = $this->addonsTable->getAddon( $this->params('uuid') );

        $this->checkOwner($this->params('uuid'), $addon);

        $this->updateNavLabels( $addon->name );

        $form = new EditAddonForm();
        $request = $this->getRequest();

        $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();

        if ( $request->isPost() )
        {
            $postdata = array_merge_recursive( $request->getPost()->toArray(),
                                               $request->getFiles()->toArray() );
            $form->setData($postdata);

            if ( $form->isValid() )
            {
                /* merge filtered */
                $hydrator->hydrate( $form->getInputFilter()->getValues(), $addon );

                if ( ($thumbnailfile = $form->getInputFilter()->getValue('thumbnail'))
                      && $thumbnailfile['tmp_name'] )
                {
                    $thumbnail = file_get_contents( $thumbnailfile['tmp_name'] );
                    $resizer = new \Addons\Filter\PNGResizer( array( 'width' => 256, 'height' => 256 ));
                    $addon->imagedata = $resizer->filter($thumbnail);
                }

                $this->addonsTable->saveAddon( $addon );
            }

        }
        else
        {
            /* we fill form from database */
            $data = $hydrator->extract( $addon );
            $form->setData( $data );
        }

        return array('addon' => $addon,'form' => $form);
    }

    public function editfilesAction()
    {
        if ( !$this->addonsTable || !$this->addonsFileTable )
        {
            $sm = $this->getServiceLocator();
            if ( !$this->addonsTable )
                $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
            if ( !$this->addonsFileTable )
                $this->addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
        }

        $addon = $this->addonsTable->getAddon( $this->params('uuid') );

        $this->checkOwner( $this->params('uuid'), $addon );

        $this->updateNavLabels( $addon->name );

        $form = new EditAddonFilesForm();
        $request = $this->getRequest();
        $data = array();

        if ( $request->isPost() )
        {
            $postdata = $request->getPost()->toArray();
            $filesdata = $request->getFiles()->toArray();
            /* Can't merge directly due to ZF bug exception */

            $form->setData($postdata);

            if ( $form->isValid() )
            {
                $filevalidator = new \Zend\Validator\File\UploadFile();
                foreach( $postdata['resources'] as $k => $v )
                {
                    $addonFile = new \Addons\Model\AddonFile();
                    $v['ref_uuid'] = $addon->uuid;

                    if ( !empty( $v['delete'] ) )
                    {
                        $addonFile->exchangeArray( $v );
                        print_r($v);
                        $this->addonsFileTable->deleteFile( $addon->uuid, $v['oldname'] );
                        unset( $postdata['resources'][$k] );
                    }
                    else if ( !empty($v['filename']) && !strstr( $v['filename'], '..' ) )
                    {
                        $file = $filesdata['resources'][$k]['fileupload'];
                        if ( $filevalidator->isValid( $file ) )
                            $v['data'] = file_get_contents( $file['tmp_name'] );
                        $addonFile->exchangeArray( $v );
                        $this->addonsFileTable->saveFile( $addonFile, $v['oldname'] );
                    }
                }

            }


        }
        else
        {
            $addonfiles = $this->addonsFileTable->fetch( $addon->uuid );
            $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty();

            /* we fill form from database */
            foreach( $addonfiles as $addonfile )
            {
                $filedata = $hydrator->extract( $addonfile );
                $filedata['oldname'] = $filedata['filename'];
                $data['resources'][] = $filedata;
            }
            if ( !empty($data['resources']) )
                $form->get('resources')->setCount( max( count($data['resources']), 1 ) );
            $form->setData( $data );
        }

        return array('addon' => $addon,'form' => $form, 'navigation' => $this->navigation );
    }

    public function ajaxAction()
    {
        $request = $this->getRequest();
        if ( !$this->addonsTable || !$this->addonsFileTable )
        {
            $sm = $this->getServiceLocator();
            if ( !$this->addonsTable )
                $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
            if ( !$this->addonsFileTable )
                $this->addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
        }

        $addon = $this->addonsTable->getAddon( $this->params('uuid') );
        $this->checkOwner( $this->params('uuid'), $addon );

        $return = array( 'status' => 'ok' );
        try
        {
            switch ($this->params('query'))
            {
                case 'remove':
                    $this->addonsFileTable->deleteFile( $addon->uuid, $request->getPost('filename') );
                    break;
                case 'properties':
                    if ( !$this->isAllowed('addon', 'inspect') )
                            throw new \Exception("Not allowed");
                    if ( $addon->getValidator('status')->isValid($request->getQuery('newstate')) )
                    {
                        $this->addonsTable->updateStatus($addon->uuid, $request->getQuery('newstate'));
                        $return['newstate'] = $request->getQuery('newstate');
                    }
                    else
                        throw new \Exception("Invalid status");
                    break;
                case 'filecontents':
                    if ( !$this->isAllowed('addon', 'inspect') )
                            throw new \Exception("Not allowed");
                    $row = $this->addonsFileTable->getFile( $addon->uuid, $request->getQuery('filename') );
                    if ( !empty($row) )
                    {
                        if (is_resource($row->data) )
                            $return['content'] = stream_get_contents($row->data);
                        $return['content'] = htmlspecialchars($return['content'], ENT_COMPAT|ENT_SUBSTITUTE, "UTF-8");
                    }
                    break;
                default:
                    throw new \Exception("Unkown query");
                    break;
            }
        } catch (\Exception $e)
        {
            $return['status'] = "error";
            $return['message'] = $e->getMessage();
            $this->getResponse()->setStatusCode(500);
        }
        $json = new \Zend\View\Model\JsonModel($return);
        return $json;
    }

    public function uploadAction()
    {
        $form = new UploadAddonForm();
        $request = $this->getRequest();
        $errmessage = "";

        if ( $request->isPost() )
        {
            $postdata = array_merge_recursive( $request->getPost()->toArray(),
                                               $request->getFiles()->toArray() );

            $form->setData( $postdata );

            if ( $form->isValid() )
            {
                $file = $form->getInputFilter()->getValue('fileupload');

                try
                {
                    $vlp = new \Addons\Model\Archive();

                    $addon = $vlp->unpack( $file['tmp_name'] );

                    $sm = $this->getServiceLocator();
                    $addonsTable = $sm->get('Addons\Model\AddonsTable');

                    $storedaddon = $addonsTable->getAddon( $addon->uuid );
                    if ( $storedaddon && $storedaddon->ref_userid != $this->zfcUserAuthentication()->getIdentity()->getId() )
                        throw new Exception("Trying to write someone's else addon");
                    unset( $storedaddon );

                    $addonsTable->saveAddon( $addon );

                    foreach( $addon->resources as $file )
                    {
                        $addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
                        $addonsFileTable->saveFile( $file, $file->filename );
                    }

                    $errmessage = "Your addon package has been successfully registered";
                }
                catch (\Exception $exc)
                {
                    $errmessage = $exc->getMessage();
                }

            }
        }

        return array('form' => $form, 'errmessage' => $errmessage);
    }
}
